<!-- .slide: data-background-video="img/intro420.mp4" -->
<!-- .slide: data-background-video-loop="true" -->

---
<!-- .slide: data-background-image="img/mike-petrucci-607505-unsplash.jpg" -->
<!-- .slide: class="invert-slide" -->

Note:
* Fifty years ago this June, Neil Armstrong and Buzz Aldrin walked on the surface of the moon.  
* But this was a feat the required amazing effort of hundreds and thousands of others

---

## Failure is not an option
### What Apollo 11 can teach us about DevSecOps

<br/><br/<br/>

Brendan O'Leary

Developer Evangelist @ &nbsp;<i class="fab fa-gitlab gitlab-orange-text"></i>

Note:
Introduction

---
<!-- .slide: data-background-image="img/erwan-hesry-500094-unsplash.jpg" -->
<!-- .slide: class="invert-slide" -->

## Context

Note:
First, let's talk about a little context to the 1960s when the space race started, and ended in 1969.

---
<!-- .slide: data-background-image="img/tv.jpg" -->

Note:
* It was a decade before Microsoft
* 40 years before Marc Andreessen said “software is eating the world”

---
<!-- .slide: data-background-image="img/2001-space-odyssey.jpg" -->
<!-- .slide: class="invert-slide" -->

Note:
2001 A Space Odyssey came out the year before the moon landing in 1968.

---
<!-- .slide: data-background-image="img/woodstock.jpg" -->
<!-- .slide: class="top-bg-img" -->

Note:
Things that happened after the moon landing include:
* Woodstock (August that year)
* Brady Bunch (September)
* Sesame Street (November)

---
<!-- .slide: data-background-image="img/bradybunch.jpg" -->

Note:
Things that happened after the moon landing include:
* Woodstock (August that year)
* Brady Bunch (September)
* Sesame Street (November)

---
<!-- .slide: data-background-image="img/sesamestreet.jpg" -->

Note:
Things that happened after the moon landing include:
* Woodstock (August that year)
* Brady Bunch (September)
* Sesame Street (November)

---
<!-- .slide: data-background-image="img/celebs-adult.png" -->

Note:
1969 also was the year that these folks were born

---
<!-- .slide: data-background-image="img/celebs-kids.png" -->

Note:
Jennifer Aniston and Jennifer Lopez.  Gwen Stefani and Jay-Z.  Paul Rudd and Marylin Manson were all born in 1969

---
<!-- .slide: data-background-image="img/artur-teixeira-530938-unsplash.jpg" -->

Note:
* 238,900 miles away
* The original specifications for Apollo didn’t mention the word “software”
* And that's an important point, because of how a small group at MIT saw the world

---

## MIT Instrumentation Laboratory

Note:
* Originally when I started writing this talk, I was focused on the MIT lab that designed the Apollo Guidance Computer.

---

![](img/dsky.jpg)

Note:
* Also called "DISS-key" (DSKY)
* Display and keyboard

---

![](img/dsky_web.png)

Note:
* Thought, oh I'll demo doing some DevOps-y things on the web version
* But then I realized how much bigger this story was

---

#### MIT Instrumentation Laboratory
## Margaret Hamilton

<img src="img/hamilton.jpg" width="450" class="plain" />

Note:
* The eventual director of the lab, Margaret Hamilton
* Born in Indiana in 1936
* Loved math because she didn't want to memorize facts, she wanted to reason them
* She said because she was "lazy"
* I realized then it was really a story about Margaret Hamilton and her contributions to our field

---
### Margaret Hamilton

<table>
	<tr class="fragment fade-left">
		<td class="gitlab-orange-text">
			Software Engineering
		</td>
	</tr>
	<tr class="fragment fade-left">
		<td class="gitlab-orange-text">
			End-to-end testing
		</td>
	</tr>
	<tr class="fragment fade-left">
		<td class="gitlab-orange-text">
			Asynchronous software
		</td>
	</tr>
	<tr class="fragment fade-left">
		<td class="gitlab-orange-text">
			Priority scheduling
		</td>
	</tr>
	<tr class="fragment fade-left">
		<td class="gitlab-orange-text">
			Agile?
		</td>
	</tr>
	<tr class="fragment fade-left">
		<td class="gitlab-orange-text">
			Source control management?
		</td>
	</tr>
</table>

Note:
Things that we invented by the MIT teams under her guidance: 
* software engineering as a discipline, end to end testing, async software, priorities, 
* but also agile maybe SCM maybe?

---

## Engineer

Note:
* It may seem trivial now, many companies use this term interchangeably with others.
* But for Hamilton, it signified something important.  Taking the software seriously

---

<img src="img/voting_software_2x.png" width="500" alt="XKCD 2030: Voting Software"/>

#### xkcd.com/2030

Note:
* But in reality, we sometimes lose sight of that term in the industry
* This XKCD captures that beautifully.
* It may be hard to read so I'll walk you through it really quickly

---

<img src="img/voting_software_slide1.png" width="400" alt="XKCD 2030: Voting Software"/>

#### xkcd.com/2030

Note:
* First we ask aircraft designers about airplane safety
* Of course, flying is the safest way to travel by far given the redudancies in modern airliners

---

<img src="img/voting_software_slide2.png" width="400" alt="XKCD 2030: Voting Software"/>

#### xkcd.com/2030

Note:
* Next we interview building engineers about elevator safety
* Basically the failsafes in moder elevators mean they are almost incapable of falling

---

<img src="img/voting_software_slide3.png" width="300" alt="XKCD 2030: Voting Software"/>

#### xkcd.com/2030

Note: 
* and finally software engineers are asked about computerized voting
* which, as you all know, is a horrible and scary idea

---

<img src="img/voting_software_slide4.png" width="900" alt="XKCD 2030: Voting Software"/>

#### xkcd.com/2030

Note:
* So much so that they recommend burying whatever it is in the desert...wearing gloves
* because as they so elequently put it: "I don't know how to say this b ut our entire field is bad at what we do"
* "If you rely on us, everyone will die"

---

### Software Engineer
### DevOps Engineer
### Site Reliability Engineer
### Test Automation Engineer
### ...

Note:
* So no matter what your title, it is important to credit Hamilton with the creation of the discipline.
* That kind of focused scientific thought can help us capture what we've been missing

---

### Engineer

<table>
	<tr class="fragment highlight-green" data-autoslide="1500">
		<td class="gitlab-yellow-text">
			Error Prevention
		</td>
		<td>
			Fault tolerance
		</td>
	</tr>
	<tr class="fragment highlight-green" data-autoslide="1500">
		<td class="gitlab-yellow-text">
			Repeatability
		</td>
		<td>
			Adaptability
		</td>
	</tr>
	<tr class="fragment highlight-green" data-autoslide="1500">
		<td class="gitlab-yellow-text">
			Availability
		</td>
		<td>
			Reliability
		</td>
	</tr>
	<tr class="fragment highlight-green">
		<td class="gitlab-yellow-text">
			Resilience
		</td>
		<td>
			Scalability
		</td>
	</tr>
</table>

Note:
True engineering involves understanding the balance and trade-offs between:
* Error prevention and fault tolerance
* Repeatability and adaptability
* Availability and reliability
* and resilience and scalability

These are relevant for everyone involved in software development.

---

## End-to-end testing

Note:
* End to end testing was nothing like it was today.
* Before I tell a testing story, let's talk about the way the computer worked.
* Hamilton: "We couldn't run something on the moon, but we could run lots of test on the ground."
* Let's talk about the AGC

---

### Apollo Guidance Computer

<small class="gitlab-orange-text">Highly Recommend: bit.ly/AGC-video</small>

- 1.024 MHz <!-- .element: class="fragment" -->
- 16 bit word length (15 data and one parity) <!-- .element: class="fragment" -->
- Runtime interpreted language - a virtual machine <!-- .element: class="fragment" -->
- Pinball Shell and remote Shell <!-- .element: class="fragment" -->

---

### Apollo Guidance Computer

<small class="gitlab-orange-text">Highly Recommend: bit.ly/AGC-video</small>

- 70 pound "portable" computer <!-- .element: class="fragment" -->
- One of the first to use integrated circuits (rather than transistors) <!-- .element: class="fragment" -->
- First computerized onboard navigation system to be used by humans and remotely <!-- .element: class="fragment" -->
- LOL Memory <!-- .element: class="fragment" -->

Note:
* Not quite as portable as our computers - but every pound counts on a rocket
* First "fly-by-wire" system that is the precursor to today's autopilot
* "little old lady" memory...which I want to talk about for a minute because its cool

---

<h3><strike>LOL Memory</strike></h3>
<h3>Core Rope Memory</h3>

<img src="img/lolmemory.jpg" width="550"/>

Note:
* This memory was physically wired with magnetic coils
* Through the coil was a ONE, around it was a ZERO
* Called little old lady because it was weaved by seamstresses

---

### Core Rope Memory

<img src="img/ropememory.jpg" width="500"/>

Note:
* You can imagine it would take months to "ship to production"
* Thus, testing was critical ahead of "releasing" your code

---

<img src="img/Margaret_Hamilton_in_action.jpg" width="750"/>

Note:
* Hamilton understood this
* Spent hours in the simulator herself testing the software
* but she was also a working mom

---

<img src="img/lauren.jpg" width="650"/>

Note:
* Her daughter - 5 or 6-year-old Lauren came with her on nights and weekends
* Wanted to "play astronaut." 
* Types "P01"
* Ended up placing the command module into "launch" mode while in mid-flight
* Hamilton recommended they place protection in to prevent it
* "Astronauts don't make mistakes."

---
<!-- .slide: data-background-image="img/earthrise.jpg" -->
<!--<img src="img/earthrise.jpg" width="650"/> -->

Note:
* The NEXT mission - Apollo 8
* Gave us "Earthrise" as the first manned mission to orbit the moon
* However, halfway there an astronaut (Jim Lovell) put in "P01"
* Erased all of the telemetry data from memory, took hours to replace
* There was a program note "do not select P01 during flight"
* Next mission had a protection in place

---

## Asynchronous software
## Priority scheduling

---
<!-- .slide: data-background-image="img/apollo11.jpg" -->

Note:
* Apollo 11
* Launched July 16 at 9:32 in the morning
* 12 minutes into Earth orbit

---
<!-- .slide: data-background-image="img/nasa-43564-unsplash.jpg" -->

Note:
* Takes about 4 days to make the transit
* 238,900 mi
* 384,472 kilometers

---
<!-- .slide: data-background-image="img/nasa-45068-unsplash.jpg" -->

Note:
* July 20th, Aldrin and Armstrong make their way into the lunar landing module 
* Start down towards the moon, but had the ground radar switch in the wrong position
* Hamilton's priority alarm system alerted the astronauts to a problem (program alarm 1201)
* Prioritized the most critical items, crew on ground recognized it and said "Go" for landing

---

<img src="img/aldrin.jpg" class="plain" />

Note:
* They landed in the sea of tranquility with 17 seconds of fuel left
* The rest is history

---

### Hamilton
 
> I remember thinking:

> 'Oh my God, it worked'

Note:
* I was so happy. But I was more happy about it working than about the fact that we landed.

---
<!-- .slide: data-background-image="img/moon-landing.jpg" -->

Note:
* But I think that's not even the whole story
* I think that the very foundation of all we do today was formed in those extra-ordinary times
* Async software, priority scheduling, and even the invention of the term "software engineer" are all on Hamilton's Wikipedia page
* But what if Hamilton and the team at MIT instrument labs invented more than that?

---

## Agile?

---

<img src="img/draper.png" class="plain" width="650" />

#### "Continous improvement retrospective"

Note:
* 17 years before the term "scrum" was invented
* "The very way you communicate can cause or prevent crashes"
* What were we doing wrong so we could stop doing it and what were we doing right so we could keep doing it?
* Define an error: is it catastrophic?  Who caused it?  How bad is it?

---

## Source control management?

---

### Source control management

* Made a change to the "main line" <!-- .element: class="fragment" -->
* Line at her door the next day <!-- .element: class="fragment" -->
* Solution: Never put change on the official version <!-- .element: class="fragment" -->
* First, you tested it on an "offline version" <!-- .element: class="fragment" -->

Note:
* The idea of "branching"
* Hard copied memos with change logs

---

<img src="img/Margaret-Hamilton.jpg" class="plain" width="500" />

Note:
* In fact, the most popular photo of her is next to a hard copy of the code
* 420,837 lines of code (about 25% comments)
* 11,0000 pages

---
<!-- .slide: data-background-image="img/thomas-kinto-1221870-unsplash.jpg" -->

Note:
* So what can we learn from all of this?
* How can we go back to the founding mothers and fathers of our industry to learn from their triumphs?
* What is our moon shot?  and how can we get there?

---
<!-- .slide: data-background-image="img/max-nelson-492729-unsplash.jpg" -->
<!-- .slide: class="invert-slide" -->

### Don't assume the customer knows what they want <!-- .element: class="fragment" -->

### Don't assume that you know your user  <!-- .element: class="fragment" -->

Note:
* NASA didn't know it needed first class software
* Don't assume your users are experts OR novices

---
### Test like a kid playing astronaut <!-- .element: class="fragment" -->

<img src="img/lego.jpeg" class="plain" width="700"/>

Note:
Test with the
* freedom
* audacity
* creativity
of a kid playing

---
<!-- .slide: data-autoslide="2000" -->
<img src="img/margaret-hamilton-mit.jpg" class="plain" width="700"/>

Note:
Remember we are standing on the shoulders of true giants

---
<!-- .slide: data-autoslide="2000" -->
<img src="img/001_3-featured.jpg" class="plain"/>

Note:
Remember we are standing on the shoulders of true giants

---
<img src="img/Margarethamilton-witch.jpg" class="plain"/>

Note:
Whoops sorry, wrong Margaret Hamilton.

---
<img src="img/hamiltonschulyersisters.jpg" class="plain"/>

Note:
Still wrong Hamilton.  Hold up

---
<!-- .slide: data-autoslide="3000" -->
<img src="img/freedom.png" class="plain"/>

Note:
Remember we are standing on the shoulders of true giants

---
<img src="img/minifig.png" class="plain"/>

Note:
Remember we are standing on the shoulders of true 

---
### Thank you, Margaret Hamilton
<img src="img/margarethamilton.jpg" class="plain" width="1000"/>

Note:
* Remember to say thank you to Margaret Hamilton.
* How can we best do that?

---

## Engineering Mindset

Note:
* An engineering mindset
* She knew what was at stake.  
* She was too "lazy" to guess
* She taught us all how to approach software and systems engineering as a true science.
* ...and because of that...

---
<!-- .slide: data-background-image="img/margaret-quote.png" -->
<!-- <img src="img/margaret-quote.png" class="plain" /> -->

Note:
* She got to be one of the biggest pioneers in our field
* No time to be beginners.

---

#### https://brendan.gitlab.io/apollo11/
<i class="fab fa-twitter" style="color: #00aced;"></i> 
<a href="https://twitter.com/olearycrew">olearycrew</a>
&nbsp;&nbsp;&nbsp;&nbsp;
<i class="fab fa-gitlab gitlab-orange-text"></i> 
<a href="https://gitlab.com/brendan">brendan</a>

<img src="img/gitlabfan-smile.png" class="plain">

Note:
* Thank you

---

![](img/error1201.png)

<span class="gitlab-orange-text">https://github.com/chrislgarry/Apollo-11</span>
<span class="gitlab-orange-text">http://svtsim.com/moonjs/agc.html</span>

---

<small>
This is literally just a slide of gifs.
</small>

<table>
	<tr>
		<td align="center">
			<img src="img/gifs/1.gif" height="125" />
		</td>
		<td align="center">
			<img src="img/gifs/2.gif" height="125"/>
		</td>
		<td align="center">
			<img src="img/gifs/3.gif" height="125"/>
		</td>
	</tr>
	<tr>
		<td align="center">
			<img src="img/gifs/4.gif" height="125"/>
		</td>
		<td align="center">
			<img src="img/gifs/5.gif" height="125"/>
		</td>
		<td align="center">
			<img src="img/gifs/6.gif" height="125" />
		</td>
	</tr>
	<tr>
		<td align="center">
			<img src="img/gifs/7.gif" height="125"/>
		</td>
		<td align="center">
			<img src="img/gifs/8.gif" height="125" />
		</td>
		<td align="center">
			<img src="img/gifs/9.gif" height="125"/>
		</td>
	</tr>
</table>

---

Photo & Video Credits

<small>


- Nasa
- Unsplash
- Some Google Images...
- Giphy !!!


</small>