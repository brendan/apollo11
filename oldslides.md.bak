
---

<pre><code class="hljs" data-line-numbers="4,8-11">
import React, { useState } from 'react';
 
function Example() {
  const [count, setCount] = useState(0);
 
  return (
    <div>
      <p>You clicked {count} times</p>
      <button onClick={() => setCount(count + 1)}>
        Click me
      </button>
    </div>
  );
}
</code></pre>

---
<!-- .slide: data-background-image="img/nosedive.jpg" -->
<!-- .slide: class="top-bg-img" -->
<!-- .slide: data-autoslide="2500" -->

Note:
* I swear I'll tie this into my opnion on DevOps, but first let's take a little journey though Black Mirror's universe
* Nosedive
* Social media/ranking leads to loss of our humanity
* Chasing likes doesn't align us to the values that make for a good person 


---
<!-- .slide: data-background-image="img/kelly-sikkema-189822.jpg" -->

## Title Slide
## Such engaging

Note:
* Foo

---

## Epsiode 2

#### Meltdown and Spectre

![Meltdown and Spectre](img/meltdownandspectre.png)

Note:
* No matter what your plans are around security, meltdown and spectre made us all rethink security
* I'm assuming many of us saw the presentation yesterday from Barbara?
* If you didn't a quick summary of Meltdown/Spectre
* Steal data from other applications in memory...the one place you don't worry about in programming


---
<!-- .slide: data-background-image="img/piotr-chrobot-278530.jpg" -->
<!-- .slide: class="invert-slide" -->

## Episode 4

#### The Illusion of Safety

Note:
PAUSE

We open on children playing on the play ground.  Their father laughs as they run around in circles.  Suddenly his phone rings.  He sees who it is and immediatly his face goes sour.  He thinks for a minute and then lowers the phone.  As soon as he's done that, it starts ringing again.  He picks it up and as he answers it everything around him fades to a blurry black.  He's not in the park at all - he's in the office all weekend.  He's been trying to figure out since early Friday which security patches apply to which parts of his infrastructure.  They had a security plan with alerts.  But they've all failed him - and he doesn't want to be in front of Congress explaining why they didn't patch a known issue for 2 years.

---
<!-- .slide: data-background-image="img/daniel-ruyter-53648-unsplash.jpg" -->

Note:
We open on children playing on the play ground.  Their father laughs as they run around in circles.  Suddenly his phone rings.  He sees who it is and immediatly his face goes sour.  He thinks for a minute and then lowers the phone.  As soon as he's done that, it starts ringing again.  He picks it up and as he answers it everything around him fades to a blurry black.  He's not in the park at all - he's in the office all weekend.  He's been trying to figure out since early Friday which security patches apply to which parts of his infrastructure.  They had a security plan with alerts.  But they've all failed him - and he doesn't want to be in front of Congress explaining why they didn't patch a known issue for 2 years.



---
<!-- .slide: data-background-image="img/david-werbrouck-304966.jpg" -->
<!-- .slide: class="invert-slide" -->

## Episode 5

#### Sisyphean Deployment

Note:
PAUSE 

The fixes and features a customer wants are done.  They've been done for weeks.  But we keep going back over them in meeting after meeting: a groundhog day of hell where no one can figure out how to bring it all together into one release and get it out the door.  Every morning the alarm clock goes off and Molly grunts disapprovingly.  She slowly starts crying to no one at all as she knows this day will not bring relief from integration, release and "scrum" meetings that go on for hours.


---
<!-- .slide: data-background-image="img/freddie-collins-309833-unsplash.jpg" -->

Note:
Ned is awake before his alarm.  The need for his powers can awaken him from even the deepest slumber.  Somewhere in the world, some one needs him.  He puts on his bright red cape and the rest of his suit.  At work, everyone knows he's a super hero.  He always has a new tool for any problem that comes up.

But in reality, that's not how his peers see him.  They see DevOps as a cost center, and Ned as the tax collector.  For each new problem, his solution is always another tool.  More money and time spent integrationg them, and longer time to deliver software.

---

<h3>It's not too late</h3>
<h3 class="fragment">It doesn't have to be this way</h3>
<h5 class="fragment" style="color: #FC9403;">With great power comes great responsiblity </h5>

---
<!-- .slide: data-background-image="img/dardan-mu-268793.jpg" -->

Note:
* There are solutions to all of these problems.  We don't have to let these problems become our future.
* We've been given a gift today.  An glimpse of what could be.  A chance to make a better future for us, our development collegues and our customers. 

---
<!-- .slide: data-background-image="img/freddie-collins-309833-unsplash.jpg" -->
<!-- .slide: class="invert-slide" -->
## Avoid the DevOps Tax 💸

Note:
* All of these items boil up to avoiding the DevOps Tax
* Be seen as a strategic advantage by the business, not a cost center constantly asking for new toys

---
<!-- .slide: data-background-image="img/scott-webb-403356-unsplash.jpg" -->
<!-- .slide: class="invert-slide" -->

## Be the change you seek 🦊
  - Reduce cycle time
  - Minimum Viable Change (MVC)
  - Lead the charge

Note:
* In order to do that, I think the most crtical thing is for us to take responsiblity for these changes in viewpoint

---
<!-- .slide: data-background-image="img/dardan-mu-268793.jpg" -->
<!-- .slide: class="invert-slide" -->

### What can you do TODAY?

* **Don't** interface ➡️ **do** integrate
* **Do** add more value than "automated builds" 🤖
  - Provide self-service to development staff 🤳
* **Do** Monitor all the things 📊

Note:
* Don't interface with things like security.  Integrate them into the pipeline
* Don't let the org think DevOps is just automated builds
* Provide self-service. That will lead to Developer Driven Pipelines
* Do orchestrate monitoring to complete the feedback loop

---
## One with security

![One with the force](img/onewiththeforce.png)

Note:
My wife told me I couldn't present at a 'tech conference' without a Star Wars reference. 

---
## Shift Left

- Move security left in the pipeline <!-- .element: class="fragment" -->
- Should not be an after thought <!-- .element: class="fragment" -->
- MUST to be automated <!-- .element: class="fragment" -->

<p class="fragment">
Do not make it a burden on teams, <br/> 
but an *enabler* of speed
</p>

<h3 class="fragment">High performing teams <span style="color: #fc6d26;">git</span> it</h3>

- 50% less time remediating security issues <!-- .element: class="fragment" -->
- 27% more automated tests <!-- .element: class="fragment" -->

Note:
And a Git joke for good measure

---

<!-- .slide: data-background-size="80%" -->

<img src="img/chart2.png" width="5000" />

<small>State of Application Security, 2018</small>

Note:
* Survey of security professionals who had experienced a penetration attack.
* How was the attack carried out?


---

# What should Equifax look like?

Note:
* In a truely automated SDLC, it is much simpler
* Instead of a manual alert to be missed, its fully automated
* CVE -> patch -> auto MR -> testing -> prod

---
<!-- .slide: data-background-image="img/kevin-364843.jpg" -->
<!-- .slide: class="invert-slide" -->

## Automated builds are NOT CI/CD

---

Automated builds ≠ CI/CD

Automated builds != CI/CD

Automated builds =/= CI/CD

Automated builds <> CI/CD

Automated builds `.NE.` CI/CD

Automated builds `'=` CI/CD

Note:
* Translated this to other languages to help out...
* Continuous is often lost
* Integrate code early and often
* Care about tests, care about tests passing
* D - delivery or deployment
* Automated packaging, releasing 
* Automated deployment is required to do CD

---
### Data is the new testing

What is <strike>measured</strike> monitored can be improved

![So Fetch](img/so-fetch.gif)

---
### Data is the new testing

![Graph](img/fleetload.png)

Note:
* SDLC is a life cycle - it doesn't wnet
* Monitoring must be part of the plan from the begining 
* Canary, ship then iterate, MVC

---
#### DevOps and "traditional" industries

<table>
	<tr class="fragment fade-left">
		<td class="gitlab-orange-text">
			Transporation
		</td>
		<td>
			Tesla; Ridesharing
		</td>
	</tr>
	<tr class="fragment fade-left">
		<td class="gitlab-orange-text">
			Healthcare
		</td>
		<td>
			Amazon; CVS
		</td>
	</tr>
	<tr class="fragment fade-left">
		<td class="gitlab-orange-text">
			Groceries
		</td>
		<td>
			Amazon
		</td>
	</tr>
	<tr class="fragment fade-left">
		<td class="gitlab-orange-text">
			Things
		</td>
		<td>
			IoT
		</td>
	</tr>
	<tr class="fragment fade-up">
		<td class="gitlab-orange-text">
			Your Industry
		</td>
		<td>
			Despite what anyone says
		</td>
	</tr>
</table>

Note:
* If you think "that's great for startups, but not in my industry"
* There's probably actually some Amazon industries I haven't thought of - others?
* A lot finanical services companies, energy companies

---

<img src="img/amazoneffect.png" width="850"/>

<small>
Source: https://www.bloomberg.com/graphics/2018-amazon-industry-displacement/
</small>

---
#### DevOps and "traditional" industries

<table>
	<tr>
		<td class="gitlab-orange-text">
			Transporation
		</td>
		<td>
			Tesla; Ridesharing
		</td>
	</tr>
	<tr>
		<td class="gitlab-orange-text">
			Healthcare
		</td>
		<td>
			Amazon; CVS
		</td>
	</tr>
	<tr>
		<td class="gitlab-orange-text">
			Groceries
		</td>
		<td>
			Amazon
		</td>
	</tr>
	<tr>
		<td class="gitlab-orange-text">
			Things
		</td>
		<td>
			IoT
		</td>
	</tr>
	<tr class="fragment grow">
		<td class="gitlab-orange-text">
			Your Industry
		</td>
		<td>
			Despite what anyone says
		</td>
	</tr>
</table>

Note:
* If you think "Brendan my boss doesn't get it"
* That's not their problem - its yours
* We are the DevOps professionals...we need to own it

---
### Everyone can contribute

![Mario Tanuki](img/MarioTanuki.jpg)

Note:
- Stop building silos, build bridges
- Place customer value first
- Don't let perfect get in the way of good..or even shipped

---

![Black Mirror Tweet](img/tweet.png)


---

As Kanye West said:

> [We're] living in the future so

> the present is [our] past.

Note:
* I could talk forever about DevOps.  Feel free to find me at the conference
* But if you take one thing away from this talk it should be
* Shift Left - Control your own destiny by controling the process end to tend
* When you interact with your boss, your bosses boss, your colleuges, your peers
* (next slide)

---

### TFW you Shift Left

<img src="img/gitlabfan-smile.png" class="plain">

Note:
* You can choose to use DevOps powers for good.  
* And when we do we will build a better future for ourselves, for our teams and for our world
* Thank you